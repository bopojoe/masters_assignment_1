require('dotenv').config()
const express = require('express')
const mongojs = require('./mongo');
const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args));
const app = express()
const PORT = 3000




app.get('/',  (req, res) => {

  var id = ""
  fetch('http://169.254.169.254/latest/api/token', {
    method: 'PUT',
    headers: { "X-aws-ec2-metadata-token-ttl-seconds": 21600 }
}).then(TOKEN => {
  console.log("here at token")

  id = fetch("http://169.254.169.254/latest/meta-data/instance-id", {
    method: 'GET',
    headers: { "X-aws-ec2-metadata-token": `${TOKEN}` }
  })

}).then( ()=>{
  
  res.status(200)
  res.send(`hi this is instance id : ${id}`)}
  );


 
})

app.get('/db',  async(req, res) => {
  
 var result = await mongojs.findAll(req, res);
  
  
    }
    

)
app.get('/db/:id', mongojs.findOne);

app.listen(process.env.PORT || PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`)
})
