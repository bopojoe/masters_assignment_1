const mongoose = require('mongoose');



const StationSchema = new mongoose.Schema({
        "tripduration": {type: Number, default: 0},
        "starttime": Date,
        "stoptime": Date,
        "start station id": {type: Number, default: 0},
        "start station name": String,
        "start station latitude": {type: Number, default: 0},
        "start station longitude": {type: Number, default: 0},
        "end station id": {type: Number, default: 0},
        "end station name": String,
        "end station latitude": {type: Number, default: 0},
        "end station longitude": {type: Number, default: 0},
        "bikeid": {type: Number, default: 0},
        "usertype": String,
        "birth year": {type: Number, default: 0},
        "gender": String
    },
    { collection: 'test' });

module.exports = mongoose.model('Station', StationSchema);