#!/bin/bash

echo -e "start of file \n \n \n \n " 
#start mongo
mongod &

process_id=$!

#wait 1 min 
sleep 60

echo -e "adding thing \n \n \n \n " 
# add 1 thing
mongo 127.0.0.1/some data.js

# keep mongo running
echo -e "keep mongo running \n \n \n \n " 
wait $process_id

