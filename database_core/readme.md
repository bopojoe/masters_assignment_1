# Database code

This is just a basic mongo docker container from docker registry, I have added the script to add data to this docker container,

## Replication
As the Docker container starts at a zero data point each time, the data is added at the start, this does mean that this backend data structure cannot be scaled but that was never the plan for this project, for a scalable data-structure I would use a volume that is then mounted (using -v "pathToVolume:mongoDbPathInContainer" in the docker run command) and then this volume could be replicated if needed for further scaling.

As storage can cost more than expected, it better in this instance to skip that and load the data in manually, project resources are limited and there is no need for it here. 