# Assignment_1_orourke_james_20074556

## Architecture Diagram
![diagram](/images/dia.png)


## AMI and scaling

Altogether for this project there are 2 Amazon AMI
These include:
    - webserver AMI
    - mongoDB database AMI

Only the Dockerized node application is scaled as the replication of data can cause issues when money is involved.


### Replication of DB
As the Docker container starts at a zero data point each time, the data is added at the start, this does mean that this backend data structure cannot be scaled but that was never the plan for this project, for a scalable data-structure I would use a volume that is then mounted (using -v "pathToVolume:mongoDbPathInContainer" in the docker run command) and then this volume could be replicated if needed for further scaling.

As storage can cost more than expected, it better in this instance to skip that and load the data in manually, project resources are limited and there is no need for it here.


## VPC Subnets

This project does not go overboard as it is a simple task to tag on extra subnets as you require
 - The project uses 3 subnets in total (2 public, 1 private)
 - The public subnets are in separate AZ for redundancy
 - The Db is in the private subnet in AZ 1
 - The public and private subnets are all in a /16 network (VPC) with individual /24 subnets (10.0.1.x, 10.0.2.x ...) managing their network addresses
 -  the nat-gateway manages the traffic destined for the private subnet

## Security Groups
- there are 2 security Groups for the VPC
  - one for the Private subnet that allows connection over port 27017
    - This is only accessible through the 10.0.0.0/16 network within the VPC ![sg-private](images/private-sg.png) 
  - one for the front end application that allows connection over port 80 ![sg-public](images/public-sg.png)


## Elastic load balancer
 - The Load balancer is set to work with the launch config and autoscaling group so it evenly distributes the traffic over the instances in the varies subnets


  ![load-balance](images/loadBalance.png)
   

## CloudWatch trigger Alarms
- The autoscaling group has simple scaling enablen to increase by one or decrease by one as needs be
- These scaling policies are manages by traffic coming in to the EC2 instances and as only the Front-end is scaled this should be able to spread the load coming in 

![Alarm](Alarm.png)
![Alarm-history](images/Alarm-history.png)
  

## Test traffic
- This is handled with a simple script that outputs the curl result from port 80 when hitting the Elastic load balancer
- As seen in the photo the loadbalencer alternates between the instances when doing the curl requests
- once this is run twice or three times the scaling triggers and another instance is spun up
- once the metric returns to the OK position the scaling removes a server until it hits the desired amount 

![test-traffic](images/test-traffic-80.png)

## Metrics
 - The main metric I read is the Docker-crashed metric
   - This metric gives realtime feedback on the status of the docker container running within the EC2 instance, although the Docker container will reboot, its nice to have the information available see how often the service goes down, it also helps to see the spin up time on the container  


## Additional Functionality

### Docker (Containers)
- The entire project is running within containers from start to finish, even the build process is done through containers,
  - The project is built on the CICD platform within Gitlab 
  - The Gitlab Runner is hosted within a container on my Arm64 Macbook
  - The runner then builds a container for the AMD64 linux system for easy hosting on aws and other platforms
  - once on AWS, the container is then spun up on the EC2 instance
    - This was initially pulled from the gitlab registry hosted within the project, but as Docker have reduced the rates lately this was showing to be unstable and sometimes failed to pull the container (due to login issues *timeouts*)


### seperate database
- The backend Database used is a docker container holding Mongo DB
- The data is loaded at creation of the container and then its ready for connection

